var gulp = require('gulp');
var sequence = require('run-sequence')
var sass = require('gulp-sass');
var inliner = require('gulp-inline-css');

var inputSCSS = './template-infofei/scss/**/*.scss';
var inputHTML = './template-infofei/*.html';
var outputCSS = './template-infofei/dist/css/';
var output = './template-infofei/dist/';

var optionsInline = {
	removeStyleTags: true,
	removeLinkTags: true
};

gulp.task('build', function(){
	sequence('sass', 'inlineCSS');
});

gulp.task('sass', function(){
	return gulp
	.src(inputSCSS)
	.pipe(sass({outputStyle: "expanded"}).on('error', sass.logError))
	.pipe(gulp.dest(outputCSS));
});

gulp.task('inlineCSS', function(){
	return gulp
	.src(inputHTML)
	.pipe(inliner(optionsInline))
	.pipe(gulp.dest(output));
});

gulp.task('watch', function(){
	return gulp
	.watch([inputSCSS, inputHTML], ['build']);
});

gulp.task('default', ['build', 'watch']);